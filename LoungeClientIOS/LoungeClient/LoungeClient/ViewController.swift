//
//  ViewController.swift
//  LoungeClient
//
//  Created by Damien Laughton on 17/09/2019.
//  Copyright © 2019 Damien Laughton. All rights reserved.
//

import UIKit
import SharedCode

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        label!.text = CommonKt.createApplicationScreenMessage()
    }


}

