# README #

This project should build in Android Studio/IntelliJ.

There is an `app` target for Android and a `SharedCode` target for producing an iOS framework.

### What is this repository for? ###

Use this project to learn about how to start producing KotlinNative frameworks

### Original Tutorial

This project was built following this tutorial - 

https://play.kotlinlang.org/hands-on/Targeting%20iOS%20and%20Android%20with%20Kotlin%20Multiplatform/01_Introduction

### How do I get set up? ###

just point IntelliJ/ Android Studio at the project

### Contribution guidelines ###

make a branch and start playing

### Who do I talk to? ###

Damien, Philli, Krzysztof
