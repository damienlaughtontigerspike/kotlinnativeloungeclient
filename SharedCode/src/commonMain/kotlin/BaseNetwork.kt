package com.tigerspike.loungeclient

import io.ktor.client.HttpClient

abstract class BaseNetwork {
    private val httpClient: HttpClient = HttpClient()

    fun performGET() {}
}